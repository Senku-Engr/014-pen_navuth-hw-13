import React, { Component } from "react";
import { Nav, Navbar, Form, FormControl, Button, Alert } from "react-bootstrap";
import Axios from "axios";
import queryString from "query-string";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect,
} from "react-router-dom";

const labelmargin = {
  marginTop: "20px",
  marginButtom: "20px",
  fontWeight: "bold",
};
const lefttext = {
  textAlign: "left",
  paddingLeft: "10%",
  paddingRight: "10%",
};

export default class Add extends Component {
  constructor(props) {
    super();
    var a = queryString.parse(props.location.search);
    this.state = {
      isUpdating: a.isUpdating,
      url: "",
      updateid: a.id,
    };
  }
  componentWillMount() {
    if (this.state.isUpdating == "true") {
      Axios.get(
        `http://110.74.194.124:15011/v1/api/articles/${this.state.updateid}`
      )
        .then((res) => {
          this.url.value = res.data.DATA.IMAGE;
          this.title.value = res.data.DATA.TITLE;
          this.desc.value = res.data.DATA.DESCRIPTION;
          this.setState({
            url: res.data.DATA.IMAGE,
          });
        })
        .catch((err) => {
          console.log(err);
        });
    }
  }
  updatearticle = (d) => {
    Axios.put(
      `http://110.74.194.124:15011/v1/api/articles/${this.state.updateid}`,
      {
        TITLE: d.TITLE,
        DESCRIPTION: d.DESCRIPTION,
        IMAGE: d.IMAGE,
      }
    ).then((res) => {
      console.log(res.data);
      alert(res.data.MESSAGE);
      this.props.history.push("/");
    });
  };

  addarticle = (d) => {
    Axios.post("http://110.74.194.124:15011/v1/api/articles", {
      TITLE: d.TITLE,
      DESCRIPTION: d.DESCRIPTION,
      IMAGE: d.IMAGE,
    }).then((res) => {
      console.log(res.data);
      alert(res.data.MESSAGE);
      this.props.history.push("/");
    });
  };
  onSubmit = () => {
    var url = this.url.value;
    var title = this.title.value;
    var desc = this.desc.value;
    if (url == "" || title == "" || desc == "") {
      this.wdesc.textContent = "* DESCRIPTION CANNOT BE BLANK";
      this.wtitle.textContent = "* TITLE CANNOT BE BLANK";
      this.wurl.textContent = "* URL CANNOT BE BLANK";
    } else {
      this.wdesc.textContent = "";
      this.wtitle.textContent = "";
      this.wurl.textContent = "";

      let article = {
        TITLE: title,
        DESCRIPTION: desc,
        IMAGE: url,
      };
      if (this.state.isUpdating == undefined) {
        this.addarticle(article);
        this.setState({
          url: url,
        });
      } else {
        this.updatearticle(article);
        this.setState({
          url: url,
        });
      }
    }
  };
  render() {
    return (
      <div className="container-fluid">
        <div className="row">
          <div className="col-8" style={lefttext}>
            <h1>
              {this.state.isUpdating === undefined
                ? "Add Article"
                : "Update Article"}
            </h1>
            <Form.Group>
              <Form.Label style={labelmargin}>TITLE :</Form.Label>
              <Form.Control
                type="text"
                placeholder="TITLE"
                ref={(title) => (this.title = title)}
              />
              <span
                ref={(wtitle) => (this.wtitle = wtitle)}
                style={{ color: "red" }}
              ></span>
              <br />
              <Form.Label style={labelmargin}>DESCRIPTION :</Form.Label>
              <Form.Control
                type="text"
                placeholder="DESCRIPTION"
                ref={(desc) => (this.desc = desc)}
              />
              <span
                ref={(wdesc) => (this.wdesc = wdesc)}
                style={{ color: "red" }}
              ></span>
              <br />
              <Form.Label style={labelmargin}>IMAGE URL :</Form.Label>

              <Form.Control
                type="text"
                placeholder="IMAGE URL"
                ref={(url) => (this.url = url)}
              />
              <span
                ref={(wurl) => (this.wurl = wurl)}
                style={{ color: "red" }}
              ></span>
              <br />
            </Form.Group>
            <Button
              type="submit"
              className={
                this.state.isUpdating === undefined
                  ? "btn btn-dark"
                  : "btn btn-warning"
              }
              onClick={this.onSubmit}
            >
              {this.state.isUpdating === undefined ? "Submit" : "Update"}
            </Button>
          </div>
          <div className="col-4">
            <img
              src={this.state.url === "" ? ph : this.state.url}
              alt="img"
              width="100%"
            />
          </div>
        </div>
      </div>
    );
  }
}
const ph =
  "https://i0.wp.com/blacktopengineering.com.au/wp-content/uploads/2017/03/placeholder-1.png?ssl=1";
