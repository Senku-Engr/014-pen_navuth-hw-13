import React, { Component } from "react";
import { Nav, Navbar, Form, FormControl, Button, Table } from "react-bootstrap";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Axios from "axios";
import List from "./List";

const artbtn = {
  padding: "20px",
};

export default class Homepage extends Component {
  constructor() {
    super();
    this.substring = this.substring.bind(this);
    this.delete = this.delete.bind(this);
    this.state = {
      data: [],
      updating: 1,
    };
  }

  delete = (id, index) => {
    Axios.delete(
      `http://110.74.194.124:15011/v1/api/articles/${id}
          `
    )
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
    alert(" YOU DELETED SUCCESSFULLY");
    var dd = [...this.state.data];
    if (index !== -1) {
      dd.splice(index, 1);
      // this.setState({ people: pp });
      this.setState((prevState) => {
        return (this.state.data = dd);
      });
    }
  };

  componentWillMount() {
    Axios.get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  substring = (art) => {
    let year = art.substring(0, 4);
    let month = art.substring(4, 6);
    let day = art.substring(6, 8);
    let date = day + "/" + month + "/" + year;
    let Date = [year, month, day];
    return Date.join("-");
  };

  render() {
    const list = this.state.data.map((d, index) => (
      <List
        data={d}
        getDelete={this.getDelete}
        delete={this.delete}
        index={index}
        key={d.ID}
      />
    ));

    return (
      <div className="col-12">
        <div style={artbtn}>
          <h1>Article management</h1>
          <Link as={Link} to="/add">
            <button className="btn btn-dark">Add new Article</button>
          </Link>
        </div>

        <Table responsive striped bordered hover>
          <thead>
            <tr>
              <th>#</th>
              <th>Title</th>
              <th>Desription</th>
              <th width="8%">Created Date</th>
              <th>Image</th>
              <th width="15%">Action</th>
            </tr>
          </thead>
          <tbody>{list}</tbody>
        </Table>
      </div>
    );
  }
}
