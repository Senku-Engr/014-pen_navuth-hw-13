import React, { Component } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

export default class List extends Component {
  constructor() {
    super();
    this.substring = this.substring.bind(this);
    this.getDelete = this.getDelete.bind(this);
  }
  getDelete = () => {
    this.props.delete(this.props.data.ID, this.props.index);
  };

  substring = (art) => {
    let year = art.substring(0, 4);
    let month = art.substring(4, 6);
    let day = art.substring(6, 8);
    let date = day + "/" + month + "/" + year;
    let Date = [year, month, day];
    // console.log(date)
    return Date.join("-");
  };

  render(props) {
    return (
      <tr key={this.props.data.ID}>
        <td>{this.props.data.ID}</td>
        <td>{this.props.data.TITLE}</td>
        <td>{this.props.data.DESCRIPTION}</td>
        <td>{this.substring(this.props.data.CREATED_DATE)}</td>
        <td>
          <img
            id="gg"
            src={
              this.props.data.IMAGE === null
                ? "https://user-images.githubusercontent.com/194400/49531010-48dad180-f8b1-11e8-8d89-1e61320e1d82.png"
                : this.props.data.IMAGE
            }
            width="300px"
          ></img>
        </td>
        <td>
          <Link as={Link} to={`/view/${this.props.data.ID}`}>
            {" "}
            <button className="btn btn-info">view</button>
          </Link>
          <Link as={Link} to={`/add?isUpdating=true&&id=${this.props.data.ID}`}>
            <button className="btn btn-warning">edit</button>
          </Link>
          <button className="btn btn-danger" onClick={this.getDelete}>
            delete
          </button>
        </td>
      </tr>
    );
  }
}
