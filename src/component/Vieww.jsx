import React, { Component } from "react";
import { Switch, Route, Link, match } from "react-router-dom";
import Axios from "axios";
const lefttext = {
  textAlign: "left",
  paddingLeft: "10%",
  paddingRight: "10%",
};
let view;
let id;
export default class Vieww extends Component {
  constructor() {
    super();
    this.state = {
      id: "",
      data: {},
    };
  }
  componentDidMount() {
    Axios.get(`http://110.74.194.124:15011/v1/api/articles/${id}`)
      .then((res) => {
        this.setState({
          data: res.data.DATA,
        });
      })
      .catch((err) => {
        console.log(err);
      });
  }
  render(props) {
    var idd = this.props.match.params.id;
    id = idd;

    return (
      <div className="container-fluid" style={lefttext}>
        <h1>Article</h1> {view}
        <div className="row">
          <div className="col-4">
            <img
              src={
                this.state.data.IMAGE === null
                  ? "https://www.slingshotvoip.com/wp-content/uploads/2019/12/placeholder-300x200.png"
                  : this.state.data.IMAGE
              }
              alt=""
              width="100%"
            />
          </div>
          <div className="col-8">
            <h1>{this.state.data.TITLE}</h1>
            <h3>{this.state.data.DESCRIPTION}</h3>
          </div>
        </div>
      </div>
    );
  }
}
