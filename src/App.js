import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Homepage from "./component/Homepage";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Mainpage from "./component/Mainpage";
import Add from "./component/Add";
import Vieww from "./component/Vieww";

function App() {
  return (
    <div className="App">
      <Router>
        <Mainpage />
        <Switch>
          <Route path="/" exact component={Homepage} />
          <Route path="/add" exact component={Add} />
          <Route path="/view/:id" exact component={Vieww} />

          {/* <Route
              path="/welcome"
              render={(props) => (
                <Welcome {...props} isAuth={this.state.isAuth} />
              )}
            /> */}
        </Switch>
      </Router>
    </div>
  );
}

export default App;
